import App from './App.svelte';

const sandboxId = document.getElementById('sandbox-root');

const app = new App({
	target: sandboxId,
});

export default app;